import logging

import pika


class RabbitMQ(object):
    def __init__(self, host=None, port=None, username=None, password=None):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        # amqp: // guest: guest @ localhost:5672 / % 2F
        self.url = "ampq://{username}:{password}@{hostname}:{port}".format(username=username, password=password,
                                                                           hostname=host, port=port)
        self.connection = pika.BlockingConnection(parameters=pika.connection.URLParameters(url=self.url))

    def get_message(self, queue=None):
        """
        :param queue:
        :return: body of message
        """
        channel = self.connection.channel()
        method_frame, header_frame, body = channel.basic_get(queue)
        if method_frame:
            logging.info(method_frame, header_frame, body)
            channel.basic_ack(method_frame.delivery_tag)
        else:
            logging.info('No message returned')

        return body

    def publish_message(self, queue=None, routing_key=None, body=None):
        """

        :param queue:
        :param routing_key:
        :param body:
        :return:
        """

        channel = self.connection.channel()
        result = channel.basic_publish(queue,
                                       routing_key=routing_key,
                                       body=body,
                                       properties=pika.BasicProperties(content_type='text/json', delivery_mode=1))
        if result:
            return True
        return False
