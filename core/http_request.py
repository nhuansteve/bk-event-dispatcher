from urllib.error import URLError
from urllib.parse import urljoin

import requests

DEFAULT_TIMEOUT = 5


class HttpClient(object):
    def __init__(self, base_url=None):
        self.base_url = base_url

    def _callback(self, request=None):
        response = request.json()
        return response

    def _check_url(self, url=None):
        if url is None:
            raise URLError("Missing url endpoint")
        if len(url) == 0:
            raise URLError("Missing url endpoint")
        return True

    def do_request(self, url=None, callback=None, method=None):
        self._check_url(url)
        r = requests.request(method=method, url=urljoin(self.base_url, url), timeout=DEFAULT_TIMEOUT)
        if callback is None:
            return self._callback(r)
        else:
            return callback(r)

    def do_get(self, url=None, callback=None):
        return self.do_request(url=url, callback=callback, method="get")

    def do_post(self, url=None, callback=None):
        return self.do_request(url=url, callback=callback, method="post")
