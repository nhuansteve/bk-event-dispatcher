from airflow.operators.python_operator import PythonOperator

from airflow.models import Variable
def print_context(ds, **kwargs):
    pprint(kwargs)
    print(ds)
    return 'Whatever you return gets printed in the logs'


update_shop_task = PythonOperator(
    task_id='update_shop_uninstall_mode',
    provide_context=True,
    op_kwargs={"session":None},
    python_callable=print_context)


save_app_shopdb_task = PythonOperator(
    task_id='save_appshop',
    provide_context=True,
    python_callable=print_context)

delete_app_shopdb_task = PythonOperator(
    task_id='delete_appshop',
    provide_context=True,
    python_callable=print_context)

def save_appshop(**kwargs):
    kwargs['ti'].xcom_pull()



