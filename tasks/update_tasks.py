import json
import logging
import os
import sys
from pprint import pprint

sys.path.append(os.getcwd())

from models.app_shop import AppShop
from utils.connection import create_mysql_session
from airflow.operators.python_operator import PythonOperator


def print_context(ds, **kwargs):
    pprint(kwargs)
    print(ds)
    return 'Whatever you return gets printed in the logs'


def pull_from_queue(**kwargs):
    id = 432
    queue_name = kwargs.get("queue_name")
    logging.info("Received message from queue: %s", queue_name)
    kwargs['ti'].xcom_push("event_shop_cancel", {"id":id})


def update_shop(**kwargs):
    msg = kwargs['ti'].xcom_pull("pull_from_queue", key="event_shop_cancel")
    id = msg.get('id')
    logging.info(id)
    session = create_mysql_session()
    app_shop = session.query(AppShop).filter_by(id=id).first()
    app_shop.cancel_app()
    AppShop.update(app_shop, session=session)
    logging.info("Done task")


pull_from_queue_task = PythonOperator(
    task_id='pull_from_queue',
    provide_context=True,
    retries=1,
    op_kwargs={"queue_name": "bk_event_uninstall"},
    python_callable=pull_from_queue)

update_shop_task = PythonOperator(
    task_id='update_shop_uninstall_mode',
    provide_context=True,
    retries=1,
    python_callable=update_shop)
