from pprint import pprint

from airflow.operators.python_operator import PythonOperator


def print_context(ds, **kwargs):
    pprint(kwargs)
    print(ds)
    return 'Whatever you return gets printed in the logs'


pull_message_from_rabbit = PythonOperator(
    task_id='pull_message_from_rabbit',
    provide_context=True,
    python_callable=print_context)

