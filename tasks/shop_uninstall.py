import logging

from airflow.operators.python_operator import PythonOperator

from api.braintree import BraintreeAPI
from controllers.app_shop import AppShopController
from controllers.email import EmailController
from models.app_shop import AppShop
from utils.connection import create_mysql_session, create_rabbitmq_connection

session = create_mysql_session()


def print_context():
    pass


def update_shop(**kwargs):
    id = 432
    logging.info(id)
    app_shop = session.query(AppShop).filter_by(id=id).first()
    app_shop.update_shop_uninstall()
    AppShopController.update(app_shop, session=session)
    logging.info("Done task")
    kwargs['ti'].xcom_push(key="update_shop", value={"id": id})


def cancel_subscription(**kwargs):
    msg = kwargs['ti'].xcom_pull("update_shop_uninstall", key="update_shop")
    id = msg['id']
    app_shop = session.query(AppShop).filter_by(id=id).first()

    if app_shop.is_upgrade_status:
        if app_shop.is_using_commission:
            rabbitmq_runner = create_rabbitmq_connection()
            rabbitmq_runner.publish_message('braintree_cashier', {
                "type": "uninstall_commission_charge_action",
                "appShopId": app_shop.id
            })
        # Uninstall app
        app_shop.cancel_app()
        AppShopController.update(app_shop, session=session)
        # Dispatcher app_shop_event cancelled
        # TODO
        # cancel subscription
        if app_shop.payment_gateway == AppShop.PAYMENT_GATEWAY_BRAINTREE and app_shop.get_braintree_subscription_id:
            manager = BraintreeAPI()
            # TODO
            manager.cancel_subscription(app_shop.braintree_subscription_id)

    pass


def upload_setting(**kwargs):
    msg = kwargs['ti'].xcom_pull("update_shop_uninstall", key="update_shop")
    shop_id = msg['shop_id']
    rabbitmq_runner = create_rabbitmq_connection()
    cache_id = 'update_shop_script_backend' + shop_id;
    rabbitmq_runner.publish_message('update_shop_script_backend', {
        "shopId": "uninstall_commission_charge_action",
        "appCode": None,
        "check_duplicate_cache_id": cache_id
    })


def delete_not_sent_emails(**kwargs):
    msg = kwargs['ti'].xcom_pull("update_shop_uninstall", key="update_shop")
    shop_id = msg['shop_id']
    email_manager = EmailController()
    email_manager.delete_not_sent_yet_emails_by_id([shop_id])


update_shop_task = PythonOperator(
    task_id='update_shop_uninstall',
    provide_context=True,
    python_callable=update_shop)

upload_setting_task = PythonOperator(
    task_id='Update_shop_settings',
    provide_context=True,
    python_callable=upload_setting)

cancel_subscription_task = PythonOperator(
    task_id='cancel_subscription',
    provide_context=True,
    python_callable=cancel_subscription)

tracking_event_task = PythonOperator(
    task_id='track_uninstall',
    provide_context=True,
    python_callable=print_context
)
delete_not_send_emails_task = PythonOperator(
    task_id='delete_not_send_emails',
    provide_context=True,
    python_callable=delete_not_sent_emails
)
