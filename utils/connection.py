import logging
import os
import sys

sys.path.append(os.getcwd())

from functools import wraps
from pymongo import MongoClient
from redis import StrictRedis
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from utils.load_config import load_config
from core.rabbitmq_consumer import RabbitMQ

sys.path.append(os.getcwd())

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
LOGGER = logging.getLogger(__name__)


def create_mongodb_connection(config_path="./config.yml"):
    config = load_config(config_path)
    MONGODB_HOST = config['MONGODB']['HOST']
    MONGODB_PORT = config['MONGODB']['PORT']
    MONGODB_NAME = config['MONOGODB']['DBNAME']
    client_mongo = MongoClient(host=MONGODB_HOST, port=MONGODB_PORT)

    return client_mongo.get_database(MONGODB_NAME)


def create_redis_connection(config_path="./config.yml"):
    config = load_config(config_path)
    REDIS_HOST = config['REDIS']['HOST']
    REDIS_PORT = config['REDIS']['PORT']
    client_redis = StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    return client_redis


def _create_mysql_alchemy(config):
    MYSQL_HOST = config['MYSQL']['HOST']
    MYSQL_PORT = config['MYSQL']['PORT']
    MYSQL_USERNAME = config['MYSQL']['USERNAME']
    MYSQL_PASSWORD = config['MYSQL']['PASSWORD']
    MYSQL_DBNAME = config['MYSQL']['DBNAME']
    MYSQL_RECONNECT = config['MYSQL']['RECYCLE']
    connection_link = "mysql+mysqlconnector://{user}:{password}@{host}:{port}/{schema}".format(user=MYSQL_USERNAME,
                                                                                               password=MYSQL_PASSWORD,
                                                                                               host=MYSQL_HOST,
                                                                                               port=MYSQL_PORT,
                                                                                               schema=MYSQL_DBNAME)

    engine = create_engine(connection_link, pool_pre_ping=True, pool_recycle=MYSQL_RECONNECT, pool_size=5)
    return engine.connect()


def create_mysql_session(config_path="./config.yml"):
    config = load_config(config_path)
    engine = _create_mysql_alchemy(config)
    Session = scoped_session(
        sessionmaker(autocommit=False, autoflush=False, bind=engine))
    session = Session()
    return session


def create_rabbitmq_connection(config_path="./config.yml"):
    config = load_config(config_path)
    rabbit_mq = RabbitMQ(host=config['RABBITMQ']['HOST'], port=config['RABBITMQ']['PORT'],
                         username=config['RABBITMQ']['USERNAME'], password=config['RABBITMQ']['PASSWORD'])
    return rabbit_mq


def provide_session(func):
    """
    Function decorator that provides a session if it isn't provided.
    If you want to reuse a session or run the function as part of a
    database transaction, you pass it to the function, if not this wrapper
    will create one and close it for you.
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        needs_session = False
        arg_session = 'session'
        func_params = func.__code__.co_varnames
        session_in_args = arg_session in func_params and \
                          func_params.index(arg_session) < len(args)
        if not (arg_session in kwargs or session_in_args):
            needs_session = True
            session = create_mysql_session()
            kwargs[arg_session] = session
        result = func(*args, **kwargs)
        if needs_session:
            session.expunge_all()
            session.commit()
            session.close()
        return result

    return wrapper


def provide_mongodb_conn(func):
    """
    Function decorator that provides a session if it isn't provided.
    If you want to reuse a session or run the function as part of a
    database transaction, you pass it to the function, if not this wrapper
    will create one and close it for you.
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        arg_session = 'session'
        func_params = func.__code__.co_varnames
        session_in_args = arg_session in func_params and \
                          func_params.index(arg_session) < len(args)
        if not (arg_session in kwargs or session_in_args):
            session = create_mongodb_connection()
            kwargs[arg_session] = session
        result = func(*args, **kwargs)
        return result

    return wrapper
