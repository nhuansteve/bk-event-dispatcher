# bk-event-dispatcher

Beeketing Airflow event dispatcher

# Project structure
- models: Contains MySQL models
- dags: Define all workflows
- hubs: Contains all tasks
- test: Contains unit tests
- utils: 

# Airflow Testing (https://airflow.apache.org/tutorial.html#example-pipeline-definition)
- airflow backfill dag_id -s date_time -e date_time