import logging
import os
import sys
from copy import deepcopy
from datetime import datetime

sys.path.append(os.getcwd())

# External Imports
from sqlalchemy import Column
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.sqltypes import DateTime, VARCHAR, Integer, Text, SmallInteger, DECIMAL

from utils.connection import provide_session

# Custom Imports

DeclarativeBase = declarative_base()


class AppShop(DeclarativeBase):
    __tablename__ = 'apps_shops'
    # __table_args__ = (
    #     Index('idx_xcom_dag_task_date', dag_id, task_id, execution_date, unique=False),
    # )

    # //? Free trial
    PAYMENT_FREE_TRIAL = 'free_trial'

    # // End free trial but not upgraded
    PAYMENT_END_FREE_TRIAL_1 = 'end_free_trial_1'

    # // After free trial 1 is ended
    PAYMENT_END_FREE_TRIAL_2 = 'end_free_trial_2'

    # // Upgraded
    PAYMENT_UPGRADED = 'upgraded'

    # // Limited
    PAYMENT_LIMITED_BY_PACKAGE = 'limited_by_package'

    # // Paying user
    PAYMENT_PAYING = 'paying'

    # // End free trial or upgraded but doesn't charge app any more
    PAYMENT_CANCELLED = 'cancelled'

    # // Package of app
    PACKAGE_LITE = 'Lite'

    PACKAGE_LITE1 = 'Lite1'

    PACKAGE_LITE2 = 'Lite2'

    PACKAGE_LITE3 = 'Lite3'

    PACKAGE_STARTER = 'Starter'

    PACKAGE_PRO = 'Pro'

    PACKAGE_NINJA = 'Ninja'

    PACKAGE_SAMURAI = 'Samurai'

    PACKAGE_GIANT = 'Giant'

    PACKAGE_GIANT2 = 'Giant2'

    PACKAGE_ENTERPRISE = 'Enterprise'

    PACKAGE_ENTERPRISE2 = 'Enterprise2'

    PACKAGE_ENTERPRISE3 = 'Enterprise3'

    PACKAGE_ENTERPRISE4 = 'Enterprise4'

    PACKAGE_ENTERPRISE5 = 'Enterprise5'

    PACKAGE_ENTERPRISE6 = 'Enterprise6'

    PACKAGE_ENTERPRISE7 = 'Enterprise7'

    PACKAGE_ENTERPRISE8 = 'Enterprise8'

    PACKAGE_ENTERPRISE9 = 'Enterprise9'

    PACKAGE_ENTERPRISE10 = 'Enterprise10'

    # // Package types
    PACKAGE_TYPE_MONTHLY = 'monthly'

    PACKAGE_TYPE_SEMI_YEARLY = 'semiyearly'
    PACKAGE_TYPE_YEARLY = 'yearly'

    # // Limited status
    LIMITED_STATUS_TYPE_EMAIL = 'limited_email'
    LIMITED_STATUS_TYPE_CONTACT = 'limited_contact'
    LIMITED_STATUS_TYPE_ORDER = 'limited_order'

    # // Extra charge status

    EXTRA_CHARGE_STATUS_FAIL = 'charge_fail'
    EXTRA_CHARGE_STATUS_SUCCESS = 'charge_success'
    EXTRA_CHARGE_STATUS_RESET = 'charge_reset'

    # // Payment gateway
    PAYMENT_GATEWAY_BRAINTREE = 'braintree_payment_gateway'

    PAYMENT_GATEWAY_SHOPIFY = 'shopify_payment_gateway'

    PAYMENT_GATEWAY_BRAINTREE_CUSTOM = 'braintree_payment_gateway_custom'

    # // Save to table settings const
    SETTING_EXTEND_FREE_TRIAL_KEY = 'extend_free_trial'

    PRICE_ORDER_BASED_VERSION = 'version_1'

    BRAINTREE_SUBSCRIPTION_CANCELLED_CODE = 81905

    COMPLETE_CANCEL_SUBSCRIPTION_SURVEY = 1

    INCOMPLETE_CANCEL_SUBSCRIPTION_SURVEY = 2
    CANCEL_SUBSCRIPTION_COUPON_CODE = 'CANCELSUB'

    CANCEL_SUBSCRIPTION_FREE_TRIAL_DAY = 15

    id = Column('id', Integer(), primary_key=True, nullable=False)
    app_id = Column('app_id', Integer())
    shop_id = Column('shop_id', Integer())
    shopify_token_key = Column('shopify_token_key', Text())
    upgraded = Column('upgraded', DateTime())
    recurring_app_charge_trial_ends_on = Column('recurring_app_charge_trial_ends_on', DateTime())
    is_power_user = Column('is_power_user', SmallInteger)
    last_installed = Column('last_installed', DateTime())
    enable = Column('enable', SmallInteger, nullable=False)
    recurring_app_charge_id = Column('recurring_app_charge_id', Integer())
    recurring_app_charge_billing_on = Column('recurring_app_charge_billing_on', DateTime())
    payment_status = Column('payment_status', VARCHAR(collation='utf8_unicode_ci', length=255))
    payment_status_last_updated = Column('payment_status_last_updated', DateTime(), nullable=False)
    created_at = Column('created_at', DateTime(), nullable=False)
    updated_at = Column('updated_at', DateTime(), nullable=False)
    recurring_app_charge_amount = Column('recurring_app_charge_amount', DECIMAL(precision=10, scale=2))
    custom_payment = Column('custom_payment', SmallInteger())
    written_review = Column('written_review', SmallInteger())
    completed_onboarding = Column('completed_onboarding')
    rated = Column('rated', Integer())
    hide_rating = Column('hide_rating', SmallInteger())
    completed_onboarding_at = Column('completed_onboarding_at', DateTime())
    token_key = Column('token_key', VARCHAR(collation='utf8_unicode_ci', length=255))
    is_user_view_on_boarding = Column('is_user_view_on_boarding', SmallInteger())
    package = Column('package', VARCHAR(collation='utf8_unicode_ci', length=60))
    cycle_success_order = Column('cycle_success_order', Integer())
    package_start_time = Column('package_start_time', DateTime())
    package_type = Column('package_type', VARCHAR(collation='utf8_unicode_ci', length=60))
    braintree_subscription_id = Column('braintree_subscription_id', VARCHAR(collation='utf8_unicode_ci', length=60))
    braintree_next_billing_amount = Column('braintree_next_billing_amount', DECIMAL(precision=10, scale=2))
    payment_gateway = Column('payment_gateway', VARCHAR(collation='utf8_unicode_ci', length=60))
    extra_charge_failed = Column('extra_charge_failed', SmallInteger())
    app_feature_limited = Column('app_feature_limited', SmallInteger())
    previous_payment_status = Column('previous_payment_status', VARCHAR(collation='utf8_unicode_ci', length=255))
    coupon_id = Column('coupon_id', Integer())
    custom_free_trial_day = Column('custom_free_trial_day', Integer())
    is_internal_app = Column('is_internal_app', SmallInteger(), nullable=False)
    package_price = Column('package_price', DECIMAL(precision=10, scale=2))
    cancel_subscription = Column('cancel_subscription', SmallInteger())
    in_active = Column('in_active', SmallInteger())
    using_commission = Column('using_commission', SmallInteger())
    commission_next_process_time = Column('commission_next_process_time', DateTime())
    remaining_free_day = Column('remaining_free_day', Integer())
    first_time_payment = Column('first_time_payment', DateTime())
    price_order_based = Column('price_order_based', VARCHAR(collation='utf8_unicode_ci', length=255))
    price_order_based_billing_on = Column('price_order_based_billing_on', DateTime())
    price_order_based_charge_amount = Column('price_order_based_charge_amount', DECIMAL(precision=10, scale=2))
    price_order_based_discount = Column('price_order_based_discount', DECIMAL(precision=10, scale=2))
    last_login = Column('last_login', DateTime())
    cancel_subscription_survey = Column('cancel_subscription_survey', SmallInteger())
    package_origin_price = Column('package_origin_price', DECIMAL(precision=10, scale=2))

    @staticmethod
    def days_past(current_date, compare_with=None):
        if compare_with is None:
            compare_with = datetime.now()
        past = compare_with - current_date
        return past.days

    def cancel_app(self):

        self.recurring_app_charge_billing_on = None
        self.recurring_app_charge_trial_ends_on = None
        self.payment_status = self.PAYMENT_CANCELLED
        self.custom_free_trial_day = None
        self.cancel_subscription = True

        if self.price_order_based is not None and len(self.price_order_based) != 0:
            self.price_order_based_billing_on = None
            self.price_order_based_charge_amount = None

        if self.cancel_subscription_survey == self.INCOMPLETE_CANCEL_SUBSCRIPTION_SURVEY:
            self.reset_survey_reward()
        return self
        # // Change appShop statuses

    def update_shop_uninstall(self):
        remaining_free_day = 0
        if self.payment_status == self.PAYMENT_UPGRADED and self.upgraded:
            remaining_free_day = self.days_past(self.upgraded)
            if remaining_free_day < 0:
                remaining_free_day = 0
        self.remaining_free_day = remaining_free_day
        self.enable = False
        return self

    def cancel_brain_tree_subscription(self):

        return self
    @classmethod
    def reset_survey_reward(self):
        pass


    def __repr__(self):
        return "app shop: id=%s, app_id=%s, shop_id=%s" % (self.id, self.app_id, self.shop_id)


def syncdb(cfg=None):
    logging.info('Initialising the database.')
    host, port, username, password, dbname = cfg['MYSQL']['HOST'], cfg['MYSQL']['PORT'], \
                                             cfg['MYSQL']['USERNAME'], cfg['MYSQL']['PASSWORD'], \
                                             cfg['MYSQL']['DBNAME']

    engine_new = create_engine('mysql+mysqlconnector://{username}:{password}@{host}:{port}/{dbname}?charset=utf8'
                               .format(username=username,
                                       password=password,
                                       host=host,
                                       port=port,
                                       dbname=dbname))
    # Syncdb
    DeclarativeBase.metadata.create_all(engine_new)


if __name__ == '__main__':
    from utils.load_config import load_config

    config = load_config(sys.argv[1])
    syncdb(config)
