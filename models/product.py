import logging
import sys

# External Imports
from sqlalchemy import Column
from sqlalchemy import DateTime, BigInteger, Float, VARCHAR
from sqlalchemy import create_engine

from sqlalchemy.ext.declarative import declarative_base

# Custom Imports

DeclarativeBase = declarative_base()


class ProductClassification(DeclarativeBase):
    __tablename__ = 'product_classification'
    id = Column(BigInteger, autoincrement=True, primary_key=True)
    product_id = Column(BigInteger)
    shop_id = Column(BigInteger)
    product_ref_id = Column(BigInteger)
    collection_id = Column(BigInteger)
    lv1 = Column(VARCHAR(45))
    lv2 = Column(VARCHAR(45))
    lv3 = Column(VARCHAR(45))
    category_name = Column(VARCHAR(45))
    confidence = Column(Float)
    last_check = Column(DateTime)


def syncdb(cfg=None):
    logging.info('Initialising the database.')
    host, port, username, password,dbname = cfg['MYSQL']['HOST'], cfg['MYSQL']['PORT'], \
                                     cfg['MYSQL']['USERNAME'], cfg['MYSQL']['PASSWORD'], \
                                     cfg['MYSQL']['DBNAME']

    engine_new = create_engine('mysql+mysqlconnector://{username}:{password}@{host}:{port}/{dbname}?charset=utf8'
                               .format(username=username,
                                       password=password,
                                       host=host,
                                       port=port,
                                       dbname=dbname))
    # Syncdb
    DeclarativeBase.metadata.create_all(engine_new)

if __name__ == '__main__':

    from utils.load_config import load_config
    config = load_config(sys.argv[1])
    syncdb(config)
