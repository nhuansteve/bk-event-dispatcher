import os
import sys

sys.path.append(os.getcwd())

from utils.connection import provide_mongodb_conn


class EmailController(object):
    COLLECTION_NAME = "Email"

    @provide_mongodb_conn
    def delete_by_condition(self, condition, session=None):
        cursor = session[self.COLLECTION_NAME]
        results = cursor.aggregate([
            {"$match": condition},
            {"$project": {
                "id": "$_id"
            }}
        ])
        email_ids = []
        for eid in results['result']:
            email_ids.append(eid)

        cursor.remove({"_id":{
            "$in": email_ids
        }})

    @provide_mongodb_conn
    def delete_not_sent_yet_emails_by_id(self,ids,session=None):
        if len(ids)<0:
            return False

        self.delete_not_sent_yet_emails_by_id({
            "sent":0,
            "shopId": {
                "$in": ids
            },
            "contactProgressId":{
                "$exists":True
            }
        })
        return True



