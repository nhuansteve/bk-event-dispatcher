import os
import sys

sys.path.append(os.getcwd())

import logging
from copy import deepcopy
from models.app_shop import AppShop
from utils.connection import provide_session


class AppShopController(object):
    @staticmethod
    @provide_session
    def update(obj, session=None):
        metadata = obj.__dict__
        data = deepcopy(metadata) or {}
        pk_id = data['id']
        data.pop("_sa_instance_state")
        try:
            session.query(AppShop).filter_by(id=pk_id).update(data)
            session.commit()
            logging.info("Save app_shop %s", metadata)
        except Exception as e:
            logging.error(e)
            raise Exception(e)
