# import os
# import sys
#
# sys.path.append(os.getcwd())
# from airflow.models import DAG
# from tasks.update_tasks import update_shop_task, pull_from_queue_task
# from datetime import datetime, timedelta
#
# args = {
#     'owner': 'nhuantran',
#     'start_date': datetime.now(),
#     'depends_on_past':True
# }
#
# # Define DAG
# dag = DAG(dag_id='app_update_event', default_args=args,start_date=datetime.now(),end_date=datetime.now(),
#           schedule_interval='@once')
#
# # Assign dag into task
# pull_from_queue_task.dag = dag
# update_shop_task.dag = dag
# # Define data flow
#
# pull_from_queue_task.set_downstream(update_shop_task)