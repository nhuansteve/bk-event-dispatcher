import os
import sys

sys.path.append(os.getcwd())
from airflow.models import DAG
from tasks.common_tasks import pull_message_from_rabbit
from tasks.shop_uninstall import *
from datetime import datetime, timedelta

seven_days_ago = datetime.combine(
    datetime.today() - timedelta(7), datetime.min.time())

args = {
    'owner': 'nhuantran',
    'start_date': seven_days_ago,
}

# Define DAG
dag = DAG(dag_id='app_uninstall_event', default_args=args,start_date=datetime.now(),
          schedule_interval=None)

# Assign dag into task
pull_message_from_rabbit.dag = dag
upload_setting_task.dag = dag
update_shop_task.dag = dag
cancel_subscription_task.dag = dag
tracking_event_task.dag = dag
# Define data flow
#
# pull_message_from_rabbit.set_downstream(update_shop_task)
# update_shop_task.set_downstream(save_app_shopdb_task)
# update_shop_task.set_downstream(cancel_subscription_task)
# update_shop_task.set_downstream(upload_setting_task)
# update_shop_task.set_downstream(delete_not_send_emails_task)
# update_shop_task.set_downstream(tracking_event_task)

pull_message_from_rabbit.set_downstream(update_shop_task)
update_shop_task.set_downstream(cancel_subscription_task)
cancel_subscription_task.set_downstream(upload_setting_task)
upload_setting_task.set_downstream(delete_not_send_emails_task)
delete_not_send_emails_task.set_downstream(tracking_event_task)

